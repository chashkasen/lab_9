﻿#include <SFML/Graphics.hpp>
#include <thread>
#include<chrono>

using namespace std::chrono_literals;

int main()
{
    sf::RenderWindow window(sf::VideoMode(1600, 800), L"Лабораторная 9");
    sf::CircleShape shape(100);
    shape.setPosition(200, 200);
    shape.setOrigin(100, 100);
    shape.setFillColor(sf::Color::Cyan);
    int shape_x = 200, shape_y = 200;
    shape.setPosition(shape_x, shape_y);

    sf::RectangleShape shape2(sf::Vector2f(100,200));
    shape2.setFillColor(sf::Color::Magenta);
    shape2.setPosition(500, 500);
    shape2.setOrigin(50, 100);
    int shape2_x=800, shape2_y=300;
    shape2.setPosition(shape2_x, shape2_y);

    sf::ConvexShape shape3;
    shape3.setPointCount(3);
    shape3.setPoint(0, sf::Vector2f(900, 325));
    shape3.setPoint(1, sf::Vector2f(400, 150));
    shape3.setPoint(2, sf::Vector2f(300, 400));
    shape3.setPosition(400, 55);
    shape3.setOrigin(400, 55);
    shape3.setFillColor(sf::Color::Yellow);
    int shape3_x=900, shape3_y=300;
    shape3.setPosition(shape3_x, shape3_y);

    while (window.isOpen())
    {
        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                window.close();
        }
        shape_y++;
        if (shape_y > 700)
            shape_y = 700;
        shape.setPosition(shape_x, shape_y);

        shape2_y++;
        if (shape2_y > 700)
            shape2_y = 700;
        shape2.setPosition(shape2_x, shape2_y);

        shape3_y++;
        if (shape3_y > 455)
            shape3_y = 455;
        shape3.setPosition(shape3_x, shape3_y);

        window.clear();
        window.draw(shape);
        window.draw(shape2);
        window.draw(shape3);

        window.display();

        std::this_thread::sleep_for(40ms);
    }

    return 0;
}